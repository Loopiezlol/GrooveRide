package ro.changa.grooveride.fragments;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bowyer.app.fabtoolbar.FabToolbar;

import java.util.ArrayList;
import java.util.List;

import ro.changa.grooveride.MainActivity;
import ro.changa.grooveride.R;
import ro.changa.grooveride.views.adapters.RecyclerViewAdapter;


public class SoundFragment extends Fragment {

    private FloatingActionButton fab;
    private FabToolbar footer;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sound, container, false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar = setUpToolbar();
        setUpFabToolbar();
        syncToolbarWithDrawer();
        mockRecyclerView();
    }

    private Toolbar setUpToolbar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return toolbar;
    }

    private void setUpFabToolbar() {
        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) getActivity().findViewById(R.id.collapse_toolbar);
        collapsingToolbar.setTitle("GrooveRide");

        collapsingToolbar.setCollapsedTitleTextColor(getResources()
                .getColor(R.color.white));
        collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.white));

        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        footer = (FabToolbar) getActivity().findViewById(R.id.fabtoolbar);

        footer.setElevation(20);
        footer.setFab(fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footer.expandFab();
                fab.setClickable(false);
            }
        });
    }

    private void syncToolbarWithDrawer() {
        DrawerLayout drawer = ((MainActivity) getActivity()).getDrawer();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    public void collapseToolbar() {
        if (fab.getVisibility() != View.VISIBLE) {
            footer.slideOutFab();
            fab.setClickable(true);
        }
    }

    private void mockRecyclerView() {
        List<String> mockList = new ArrayList<>();
        mockList.add("afsaagagaga");
        mockList.add("afsaagagaga");
        mockList.add("afsaagagaga");
        mockList.add("afsaagagaga");
        mockList.add("afsaagagaga");
        mockList.add("afsaagagaga");
        mockList.add("afsaagagaga");
        mockList.add("afsaagagaga");
        mockList.add("afsaagagaga");
        mockList.add("afsaagagaga");
        mockList.add("afsaagagaga");
        mockList.add("afsaagagaga");
        mockList.add("afsaagagaga");
        mockList.add("afsaagagaga");
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.my_recycler_view);

        adapter = new RecyclerViewAdapter(mockList, SoundFragment.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);


        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                collapseToolbar();
            }
        });
    }


}
