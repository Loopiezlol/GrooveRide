package ro.changa.grooveride.views.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ro.changa.grooveride.R;
import ro.changa.grooveride.fragments.SoundFragment;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

    private List<String> list;
    private SoundFragment fragment;

    public RecyclerViewAdapter(List<String> list, SoundFragment fragment) {
        this.list = list;
        this.fragment = fragment;
    }


    public void updateList(List<String> list) {

        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.sound_recycler_row_layout, viewGroup, false);
        return new RecyclerViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder viewHolder, int i) {
        viewHolder.getTextView().setText(list.get(i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        private TextView itemView;
        private CardView cardView;

        public RecyclerViewHolder(final View view) {
            super(view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(view.getContext(), "Swag", Toast.LENGTH_SHORT).show();
                    fragment.collapseToolbar();
                }
            });
            itemView = (TextView) view.findViewById(R.id.card_text_view);
            cardView = (CardView) view.findViewById(R.id.card_view);
        }

        public TextView getTextView() {
            return itemView;
        }
    }


}
